package io.hino.controller;

import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.Map;

import static org.springframework.web.util.WebUtils.ERROR_MESSAGE_ATTRIBUTE;

@RestController
public class SocialController extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        SimpleUrlAuthenticationFailureHandler handler = new SimpleUrlAuthenticationFailureHandler("/");

        http.authorizeRequests(a -> a
                .antMatchers("/", "/favicon.ico", "/error", "/webjars/**").permitAll()
                .anyRequest().authenticated()
        ).logout(l -> l
                .logoutSuccessUrl("/").permitAll()
        ).csrf(c -> c
                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
        ).exceptionHandling(e -> e
                .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))
        ).oauth2Login(o -> o
                .failureHandler((request, response, exception) -> {
                    request.getSession().setAttribute(ERROR_MESSAGE_ATTRIBUTE, exception.getMessage());
                    handler.onAuthenticationFailure(request, response, exception);
                })
        );
    }

    @GetMapping("/user")
    public Map<String, Object> user(@AuthenticationPrincipal OAuth2User principal) {
        return Collections.singletonMap("name", principal.getAttribute("name"));
    }

    @GetMapping("/error")
    public String error(HttpServletRequest request) {
        String message = (String) request.getSession().getAttribute(ERROR_MESSAGE_ATTRIBUTE);
        request.getSession().removeAttribute(ERROR_MESSAGE_ATTRIBUTE);
        return message;
    }

}
